# Mine da Gap 2020 Theme

A custom theme for [Mine da Gap](http://minedagap.com/).

## Dependencies

* [Observant Records 2015 Theme](https://bitbucket.org/observantrecords/observant-records-theme-2015-for-wordpress)
