<?php
/**
 * Created by PhpStorm.
 * User: gbueno
 * Date: 12/22/2014
 * Time: 10:37 AM
 */

namespace ObservantRecords\WordPress\Themes\MineDaGap2020;


class Setup {

	public static function init() {
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'wp_enqueue_styles'), 21);
	}

	public static function wp_enqueue_styles() {
		wp_dequeue_style( 'observantrecords2020-style' );

        wp_enqueue_style( 'minedagap2020-fonts', '//fonts.googleapis.com/css2?family=Rock+Salt&display=swap" rel="stylesheet' );
        wp_enqueue_style( 'minedagap2020-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );
	}

}